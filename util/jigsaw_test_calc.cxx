//jigsaw
#include "jigsawcalculator/JigsawCalculator.h"

//std/stl
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <map>
using namespace std;

//ROOT
#include "TLorentzVector.h"

const string algo = "test_calc";

int main()
{
    cout << algo << endl;

    jigsaw::JigsawCalculator* calculator = new jigsaw::JigsawCalculator();
    calculator->initialize("TTMET2LW");

    TLorentzVector metTLV;
    metTLV.SetPxPyPzE(43., 37., 0., 123.);
    TLorentzVector lepton0;
    lepton0.SetPtEtaPhiM(72., 1.3, 0.4, 72.);
    TLorentzVector lepton1;
    lepton1.SetPtEtaPhiM(47., -2.3, 1.4, 47);

    map<string, vector<TLorentzVector> > object_map;

    object_map["leptons"] = { lepton0, lepton1 };
    object_map["met"] = { metTLV };
    calculator->load_event(object_map);

    cout << algo << "    ---------------------------------------------------" << endl;
    cout << algo << "    Loaded variables and values for dummy event:" << endl;
    int var_idx = 0;
    for(auto vm : calculator->variables())
    {
        cout << algo <<"     > [" << std::setw(2) <<var_idx<<"] " << std::setw(15) << vm.first << " : " << vm.second << endl;
        var_idx++;
    }
    cout << algo << "    ---------------------------------------------------" << endl;

    delete calculator;
    return 0;
}

