#
# package: JigsawCalculator
#
# author: daniel.joseph.antrim@CERN.CH
#         dantrim@uci.edu
#
# November 2018
#

if("$ENV{JIGSAWDIR}" STREQUAL "")
    message(FATAL_ERROR "JIGSAWDIR environment not set!")
endif()

atlas_subdir( jigsawcalculator )

set( extra_deps )
set( extra_libs )

find_package( ROOT COMPONENTS Gpad Graf Graf3d Core Tree MathCore Hist RIO Physics )

atlas_add_library( JigsawCalculator
    jigsawcalculator/*.h jigsawcalculator/utility/*.h jigsawcalculator/calculators/*.h
    Root/*.cxx Root/utility/*.cxx Root/calculators/*.cxx
    PUBLIC_HEADERS jigsawcalculator #jigsawcalculator/utility
    PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} $ENV{JIGSAWDIR}/RestFrames/include
    LINK_LIBRARIES ${ROOT_LIBRARIES} $ENV{JIGSAWDIR}/RestFrames/lib/libRestFrames.so
)

function(JigsawExec filename)
    set(Execname)
    get_filename_component(execname ${filename} NAME_WE)
    atlas_add_executable( ${execname} "util/${execname}.cxx"
        INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} $ENV{JIGSAWDIR}/RestFrames/include
        LINK_LIBRARIES ${ROOT_LIBRARIES} JigsawCalculator $ENV{JIGSAWDIR}/RestFrames/lib/libRestFrames.so
    )
endfunction( JigsawExec )

# build everying in the util/ directory
file(GLOB files "util/*.cxx")
foreach(file ${files})
    JigsawExec(${file})
endforeach()

atlas_install_data( data/* )
