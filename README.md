# JigsawCalculator

Tool to calculate Recursive Jigsaw variables

## Installing JigsawCalculator
<details> <summary> Expand for Installation Steps </summary>

Here are the steps to take in order to install JigsawCalculator. The assumption
is that you perform these steps in your analysis directory and that you
have `cvmfs/`, the ability to setup an ```AnalysisBase``` release,
and have access to the internet. This also assumes that your analysis 
directory has the ATLAS top-level CMakeLists.txt that comes when you do
```asetup AnalysisBase,21.2.55,here``` (the ```here``` is what generates
the expected top-level CMakeLists.txt file).

The installation steps below checkout this package, `JigsawCalculator` and its
one dependency, the library [RestFrames](http://restframes.com/) which
is the library that implements the Recursive Jigsaw reconstruction algorithms.

```bash
cd <your-analysis-directory>
mkdir source/
cd source/
lsetup "asetup AnalysisBase,21.2.55,here"
git clone https://:@gitlab.cern.ch:8443/dantrim/jigsawcalculator.git
source jigsawcalculator/bash/install_restframes.sh
source jigsawcalculator/bash/setup.sh
cd ..
mkdir build/
cd build/
cmake ../source
make -j4
source x86*/setup.sh
```

Depending on your analysis directory structure, you should tailor the steps that involve the *source/* and *build/* directories,
and of course change the `AnalysisBase` release version to your needs (this package has no dependencies on ATLAS software so
the release is not important).

After sourcing `jigsawcalculator/bash/install_restframes.sh` you will have, at the same level as the package `jigsawcalculator`
a directory named *RestFrames/* which holds the `RestFrames` library.

Sourcing the script `jigsawcalculator/bash/setup.sh` sets your runtime environment so that you will be able to load the
`RestFrames` library appropriately. **This script must be called every time you wish to compile/run code that depends on `RestFrames`**

After successfully completing the above steps, you can run an example executable, `jigsaw_test_calc`, which implements
a JigsawCalculator to calculate the Jigsaw observables from the ttbar+MET,2L analysis from 2015-2016. The executable
loads a dummy event with randomly assigned leptons and met TLorentzVectors and calculates the observables from those.

The first thing you will notice is the unfortunate banner highlighting Christopher Rogan, the main
developer of `RestFrames`. Hack `RestFrames` source code as you wish to remove this.

The source code for the executable `jigsaw_test_calc` is [here](https://gitlab.cern.ch/dantrim/jigsawcalculator/blob/master/util/jigsaw_test_calc.cxx)
and the source code for the JigsawCalculator for the ttbar+MET,2L analysis is [here](https://gitlab.cern.ch/dantrim/jigsawcalculator/blob/master/Root/calculators/CalculatorTTMET2LW.cxx).

```bash
jigsaw_test_calc

RestFrames v1.0.0 -- Developed by Christopher Rogan (crogan@cern.ch)
                     Copyright (c) 2014-2016, Christopher Rogan
                     http://RestFrames.com

test_calc
jigsaw::JigsawCalculator::initialize(...)   Initializing JigsawType: TTMET2LW
test_calc    ---------------------------------------------------
test_calc    Loaded variables and values for dummy event:
test_calc     > [ 0]         DPB_vSS : 0.127596
test_calc     > [ 1]         H_11_S1 : 15.0318
test_calc     > [ 2]       H_11_S1_T : 10.2888
test_calc     > [ 3]         H_11_SS : 43.5295
test_calc     > [ 4]       H_11_SS_T : 43.5092
test_calc     > [ 5]         H_12_SS : 348.775
test_calc     > [ 6]         H_21_SS : 372.581
test_calc     > [ 7]       H_21_SS_T : 89.9237
test_calc     > [ 8]         H_22_SS : 677.827
test_calc     > [ 9]       H_22_SS_T : 142.107
test_calc     > [10]             MDR : 144.782
test_calc     > [11]             RPT : 0.472023
test_calc     > [12]     RPT_H_11_SS : 0.936959
test_calc     > [13]   RPT_H_11_SS_T : 0.936987
test_calc     > [14]     RPT_H_21_SS : 0.634562
test_calc     > [15]   RPT_H_21_SS_T : 0.877968
test_calc     > [16]     RPT_H_22_SS : 0.488352
test_calc     > [17]   RPT_H_22_SS_T : 0.819906
test_calc     > [18]             RPZ : 0.538732
test_calc     > [19]     RPZ_H_11_SS : 0.95102
test_calc     > [20]   RPZ_H_11_SS_T : 0.951041
test_calc     > [21]     RPZ_H_21_SS : 0.694045
test_calc     > [22]   RPZ_H_21_SS_T : 0.903836
test_calc     > [23]     RPZ_H_22_SS : 0.554943
test_calc     > [24]   RPZ_H_22_SS_T : 0.856063
test_calc     > [25]     costheta_SS : 0.69477
test_calc     > [26]     dphi_S_I_s1 : 2.01239
test_calc     > [27]     dphi_S_I_ss : 0.156335
test_calc     > [28]   dphi_s1_s2_ss : -3.14159
test_calc     > [29]   dphi_v1_i1_ss : 0.276015
test_calc     > [30]       dphi_v_SS : 2.47294
test_calc     > [31]       gamInvRp1 : 0.0619396
test_calc     > [32]           pTT_T : 161.741
test_calc     > [33]           pTT_Z : -211.296
test_calc    ---------------------------------------------------
```

</details>

## Adding Your Own JigsawCalculator (and how JigsawCalculator is designed)
<details> <summary> Expand </summary>

In order to add your own ```JigsawCalculator``` there are a few steps to take.

There is a single, top level interface ```jigsaw::JigsawCalculator``` which the user
builds in their code and initializes with a ```jigsaw::JigsawType```, which
is an enum defined [here](jigsawcalculator/JigsawType.h#L9). You can see this
in action in the example executable ```jigsaw_test_calc``` [here](util/jigsaw_test_calc.cxx#L21)
where we pass to the top level calculator's ```jigsaw::JigsawCalculator::initialize(std::string)```
method the string value of the ```jigsaw::JigsawType``` enum. The ```jigsaw::JigsawCalculator::initialize(std::string)```
method initializes the underlying implementation of the calculator, so if you
want to add a new JigsawCalculator you need to update ```jigsaw::JigsawCalculator::initialize(std::string)```
to handle it, as for the calculator ```jigsaw::CalculatorTTMET2LW``` [here](Root/JigsawCalculator.cxx#L32).

The various "calculators" are implementations of purely virtual base classes of
type ```jigsaw::JigsawCalculatorImp``` ([header](jigsawcalculator/JigsawCalculatorImp.h))
and so your specific implementation must inherit directly from this class and 
implement all of the methods. You can see this inheritance in ```jigsaw::CalculatorTTMET2LW```
[here](jigsawcalculator/calculators/CalculatorTTMET2LW.cxx#L33). The metods that must be
implemented are:

* ```void initialize_calculator()``` : responsible for building the Recursive Jigsaw rest-frames and tree, throw exception if fails ([TTMET2LW example](Root/calculators/CalculatorTTMET2LW.cxx#L47))
* ```bool load_event(std::map<std::string, std::vector<TLorentzVector>> object_map)``` : the method for passing in the TLorentzVectors of the objects needed to calculate the Jigsaw variables ([TTMET2LW example](Root/calculators/CalculatorTTMET2LW.cxx#L125))
* ```std::map<std::string, float> variables_map()``` : the method that returns the calculated Jigsaw variables back to the user, by name and value pairs in the ```std::map```
* ```std::vector<std::string> variable_names()``` : returns vector of variable names that are calculated by the Calculator

The map of variables returned by ```variables_map()``` is a protected member of the inherited
base class ```jigsaw::JigsawCalculatorImp``` [here](jigsawcalculator/JigsawCalculatorImp.h#L31).

So, the steps to create a new JigsawCalcular are:

* add a new JigsawType (add to [enum](jigsawcalculator/JigsawType.h#L9) and implement in the [source](Root/JigsawType.cxx))
* handle the initialization of the new JigsawType in ```jigsaw::JigsawCalculator::initialize``` [here](Root/JigsawCalculator.cxx#L32)
* implement your Calculator by implementing the methods described above and place the code under `jigsawcalculator/{Root,jigsawcalculator}/calculators/` as
in the case for the ```TTMET2LW``` calculator ([source](Root/calculators/CalculatorTTMET2LW.cxx), [header](jigsawcalculator/calculators/CalculatorTTMET2LW.h))


</details>

## Using The JigsawCalculator Library In Your Analysis Code
<details> <summary> Expand </summary>

There is an example package [JigsawCalculatorExample](https://gitlab.cern.ch/dantrim/jigsawcalculatorexample)
which shows how to compile a separate analysis package that links to the ```JigsawCalculator``` library.

</details>
