#!/bin/bash

#
# script to setup the environment variable JIGSAWDIR
# needed for finding RestFrames library during compilation
# and runtime
#
# author: daniel.joseph.antrim@CERN.CH
#         dantrim@UCI.EDU
# November 2018
#

function print_usage {

    echo "--------------------------------------------------------------"
    echo " setup JIGSAWDIR"
    echo ""
    echo " Options:"
    echo "  --restframesdir         Override the default JIGSAWDIR location where RestFrames/ is (default: directory wherein jigsawcalculator lives)"
    echo "  -h|--help               Print this help message"
    echo ""
    echo " Example usage:"
    echo "  $ source setup.sh"
    echo "--------------------------------------------------------------"

}

function main {

    calling_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
    rjdir=$( cd ${calling_dir}; cd ../..; echo "${PWD}")
    while test $# -gt 0
    do
        case $1 in
            --restframes-dir)
                rjdir=${2}
                shift
                ;;
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                ;;
            *)
                echo "ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift 
    done

    unset JIGSAWDIR
    abs_rjdir=$(readlink -f ${rjdir})
    export JIGSAWDIR=${abs_rjdir}
    source ${abs_rjdir}/RestFrames/setup_RestFrames.sh
    #export CMAKE_PREFIX_PATH=${JIGSAWDIR}:${CMAKE_PREFIX_PATH}

}


#___________
main $*
