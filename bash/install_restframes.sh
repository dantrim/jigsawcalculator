#!/bin/bash

#
# script to install the RestFrames library
#
# contact: daniel.joseph.antrim@CERN.CH
#          dantrim@uci.edu
# November 2018 
#

default_rj_tag="v1.0.1"
yes="0"

function print_usage {

    echo "--------------------------------------------------------------"
    echo " install RestFrames"
    echo ""
    echo " Options:"
    echo " -d|--build-dir               Directory to install the RestFrames library to (default: directory wherein jigsawcalculator lives)"
    echo " -t|--tag                     Tag/branch/release of RestFrames to install (default: $default_rj_tag)"
    echo " -y                           Don't ask permission to make new directories (default: false)"
    echo " -h|--help                    Print this help message"
    echo ""
    echo " Example usage:"
    echo "  Execute this:"
    echo "   $ ./install_restframes.sh"
    echo "--------------------------------------------------------------"

}

function check_build_dir {

    test_dir=${1}
    yes=${2}
    
    if [ ! -d $test_dir ]; then
        build_it="0"
        if [ ! ${yes} -eq "1" ]; then
            echo ""
            echo "install_restframes    Build directory \"$test_dir\" does not yet exist"
            echo "install_restframes     > Create it? [yes=1,no=0]"
            read build_it
            if [ ! ${build_it} -eq "1" ]; then
                echo "install_restframes    Exiting"
                return 1
            fi
        else
            build_it="1"
        fi
        if [ $build_it -eq "1" ]; then
            echo "install_restframes    Creating directory: $test_dir"
            mkdir -p $test_dir
            return 0
        fi
        echo "install_restframes    Build directory \"$test_dir\" does not exist, exitting"
        return 1
    else
        if [ -d $test_dir/RestFrames ]; then
            echo "install_restframes    ERROR RestFrames installation already found in requested build directory ($test_dir)"
            return 1
        fi
    fi

    return 0

}

function clone_repo {

    cd ${1}
    tag=${3}
    
    #git clone ${2} RestFrames
    git clone git@github.com:crogan/RestFrames.git
    if [ ! -d RestFrames ]; then
        echo "install_restframes    ERROR failed to clone RestFrames repository"
        return 1
    fi
    cd RestFrames/
    git checkout $tag
    cd ..
    return 0

}

function build_restframes {

    start=${PWD}

    restframes_dir=${1}/RestFrames
    cd $restframes_dir

    echo "build_restframes from ${PWD}"
    installdir=${PWD}
    ./configure --prefix $installdir

    make &&
    make install

    cd $restframes_dir
    source setup_RestFrames.sh

    cd $start

}
function checkout_and_build_restframes {

    build_dir=${1}
    restframes_tag=${2}
    repo_url="git@github.com:crogan/RestFrames.git"

    if ! clone_repo $build_dir $repo_url $restframes_tag ; then return 1; fi

    if ! build_restframes $build_dir ; then return 1; fi

}

function main {

    rj_tag=$default_rj_tag
    calling_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
    build_dir=$( cd ${calling_dir}; cd ../..; echo "${PWD}")

    while test $# -gt 0
    do
        case $1 in
            -d)
                build_dir=${2}
                shift
                ;;
            --build-dir)
                build_dir=${2}
                shift
                ;;
            -t)
                rj_tag=${2}
                shift
                ;;
            --tag)
                rj_tag=${2}
                shift
                ;;
            -y)
                yes="1"
                ;;
            -h)
                print_usage
                return 0 
                ;;
            --help)
                print_usage
                return 0
                ;;
            *)
                echo "ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift
    done

    startdir=${PWD}
    abs_build_dir=$(readlink -f ${build_dir})

    #(
        if ! check_build_dir ${abs_build_dir} ${yes} ; then return 1; fi
    #)

    (
        if ! checkout_and_build_restframes ${abs_build_dir} ${rj_tag} ; then return 1; fi
    )
    

    unset yes
    unset default_build_dir
    unset default_rj_tag
    unset build_dir
    unset abs_build_dir

    echo ""
    tput setaf 2
    echo "install_restframes   installation complete"
    tput sgr0

}

#_______
main $*
