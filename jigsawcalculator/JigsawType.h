#ifndef JIGSAW_JIGSAW_TYPE
#define JIGSAW_JIGSAW_TYPE

#include <vector>
#include <string>

namespace jigsaw
{
    enum JigsawType
    {
        TTMET2LW=0
        ,JigsawInvalid
    }; // enum JigsawType

    std::string JigsawType2Str(const JigsawType& type);
    JigsawType JigsawTypeFromStr(const std::string& t);
    bool IsValidJigsawType(std::string name);
    std::vector<std::string> ValidJigsawTypes();

} // namespace jigsaw

#endif
