#ifndef JIGSAW_IMP_H
#define JIGSAW_IMP_H

//jigsaw
//#include "jigsawcalculator/JigsawCalculator.h"

//std/stl
#include <string>
#include <vector>
#include <map>

//ROOT
#include "TLorentzVector.h"

namespace jigsaw
{

class JigsawCalculatorImp
{
    public :
        virtual void initialize_calculator() = 0;
        virtual std::string type() = 0;
        virtual bool load_event(std::map<std::string, std::vector<TLorentzVector>> object_map) = 0;
        virtual void fill_variables() = 0;
        virtual void add_var(std::string varname, float val) = 0;
        virtual std::map<std::string, float> variables_map() = 0;
        virtual std::vector<std::string> variable_names() = 0;

    protected :
        std::string m_type;
        std::map<std::string, float> m_variables_map;
        std::vector<std::string> m_variable_names;

}; // class JigsawCalculatorImp

} // namespace jigsaw

#endif
