//jigsaw
#include "jigsawcalculator/JigsawType.h"

//std/stl
#include <algorithm>
using namespace std;

namespace jigsaw
{

string JigsawType2Str(const JigsawType& type)
{
    string out = "JigsawInvalid";
    switch(type)
    {
        case JigsawType::TTMET2LW : out = "TTMET2LW"; break;
        case JigsawType::JigsawInvalid : out = out; break;
    } // swtich
    return out;
}

JigsawType JigsawTypeFromStr(const std::string& type)
{
    JigsawType out = JigsawType::JigsawInvalid;
    if(type == "TTMET2LW") out = JigsawType::TTMET2LW;
    return out;
}

bool IsValidJigsawType(std::string name)
{
    auto valid_types = ValidJigsawTypes();
    return (std::find(valid_types.begin(), valid_types.end(), name) != valid_types.end());
}

vector<string> ValidJigsawTypes()
{
    vector<string> out;
    for(size_t i = 0; i < JigsawType::JigsawInvalid; i++)
    {
        out.push_back(JigsawType2Str((JigsawType)i));
    }
    return out;
}


} // namespace jigsaw
