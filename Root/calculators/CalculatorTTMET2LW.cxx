//jigsaw
#include "jigsawcalculator/calculators/CalculatorTTMET2LW.h"
#include "jigsawcalculator/utility/helpers.h"

//std/stl
#include <iostream>
#include <sstream>
using namespace std;

//ROOT
#include "TVectorD.h"

//RestFrames
#include "RestFrames/RestFrames.hh"
using namespace RestFrames;

namespace jigsaw
{

CalculatorTTMET2LW::CalculatorTTMET2LW()
{
    m_variables_map.clear();
    m_variable_names.clear();
}

std::string CalculatorTTMET2LW::type()
{
    return m_type;
}

map<string, float> CalculatorTTMET2LW::variables_map()
{

    return m_variables_map;
}

vector<string> CalculatorTTMET2LW::variable_names()
{
    return m_variable_names;
}

void CalculatorTTMET2LW::add_var(string varname, float val)
{
    m_variables_map[varname] = val;
}

void CalculatorTTMET2LW::initialize_calculator()
{

    // initialize the frames
    rf_lab = std::make_unique<LabRecoFrame>("LAB", "LAB");
    rf_ss = std::make_unique<DecayRecoFrame>("SS", "SS");
    rf_s1 = std::make_unique<DecayRecoFrame>("S1", "S1");
    rf_s2 = std::make_unique<DecayRecoFrame>("S2", "S2");
    rf_v1 = std::make_unique<VisibleRecoFrame>("V1", "V1");
    rf_v2 = std::make_unique<VisibleRecoFrame>("V2", "V2");
    rf_i1 = std::make_unique<InvisibleRecoFrame>("I1", "I1");
    rf_i2 = std::make_unique<InvisibleRecoFrame>("I2", "I2");

    // connect them
    rf_lab->SetChildFrame(*rf_ss);
    rf_ss->AddChildFrame(*rf_s1);
    rf_ss->AddChildFrame(*rf_s2);
    rf_s1->AddChildFrame(*rf_v1);
    rf_s1->AddChildFrame(*rf_i1);
    rf_s2->AddChildFrame(*rf_v2);
    rf_s2->AddChildFrame(*rf_i2);

    // check that the connections between teh frames are valid
    if(!rf_lab->InitializeTree())
    {
        stringstream e;
        e << JIGFUNC << ": InitializeTree failure";
        throw std::runtime_error(e.str());
    }

    // define the groups and jigsaws
    group_inv = std::make_unique<InvisibleGroup>("INV", "Invisible Group Jigsaws");
    group_comb_vis = std::make_unique<CombinatoricGroup>("VIS", "Visible Group Jigsaws");

    group_inv->AddFrame(*rf_i1);
    group_inv->AddFrame(*rf_i2);

    group_comb_vis->AddFrame(*rf_v1);
    group_comb_vis->SetNElementsForFrame(*rf_v1, 1, false);
    group_comb_vis->AddFrame(*rf_v2);
    group_comb_vis->SetNElementsForFrame(*rf_v2, 1, false);

    // define the jigsaws

    jigsaw_invmass = std::make_unique<SetMassInvJigsaw>("MinMassJigsaw", "Invisible System Mass Jigsaw");
    group_inv->AddJigsaw(*jigsaw_invmass);

    jigsaw_rapidity = std::make_unique<SetRapidityInvJigsaw>("RapidityJigsaw", "Invisible System Rapidity Jigsaw");
    group_inv->AddJigsaw(*jigsaw_rapidity);
    jigsaw_rapidity->AddVisibleFrames(rf_lab->GetListVisibleFrames());


    jigsaw_contraboost = std::make_unique<ContraBoostInvJigsaw>("ContraBoostJigsaw", "ContraBoost Invariant Jigsaw");
    group_inv->AddJigsaw(*jigsaw_contraboost);
    jigsaw_contraboost->AddVisibleFrames( (rf_s1->GetListVisibleFrames()), 0 );
    jigsaw_contraboost->AddVisibleFrames( (rf_s2->GetListVisibleFrames()), 1 );
    jigsaw_contraboost->AddInvisibleFrame( *rf_i1, 0 );
    jigsaw_contraboost->AddInvisibleFrame( *rf_i2, 1 );

    
    jigsaw_minmass = std::make_unique<MinMassesCombJigsaw>("HemiJigsaw", "Minimize m_{v_{1,2}} Jigsaw");
    group_comb_vis->AddJigsaw(*jigsaw_minmass);
    jigsaw_minmass->AddFrame( *rf_v1, 0 );
    jigsaw_minmass->AddFrame( *rf_v2, 1 );

    // check that the Jigsaw rules are valid and setup properly
    if(!rf_lab->InitializeAnalysis())
    {
        stringstream e;
        e << JIGFUNC << ": InitializeAnalysis failure";
        throw std::runtime_error(e.str());
    }

    // clear the event for the start
    rf_lab->ClearEvent();

}

bool CalculatorTTMET2LW::load_event(map<string, vector<TLorentzVector>> object_map)
{
    // clear the event for the new objects to populate the restframes
    rf_lab->ClearEvent();

    vector<TLorentzVector> leptons;
    TLorentzVector met;
    try {
        leptons = object_map.at("leptons");
    }
    catch(std::exception& e)
    {
        stringstream s;
        s << JIGFUNC << "   ERROR Failed to load \"leptons\" from input object_map: " << e.what();
        cout << s.str() << endl;
        return false;
    }

    try {
        auto met_vec = object_map.at("met"); //.at(0);
        met = met_vec.at(0);
    }
    catch(std::exception& e)
    {
        stringstream s;
        s << JIGFUNC << "   ERROR Failed to load \"met\" from input object_map: " << e.what();
        cout << s.str() << endl;
        return false;
    }

    if(!(leptons.size() >= 2))
    {
        cout << JIGFUNC << "   ERROR At least two leptons are required for this Calculator, only got " << leptons.size() << endl;
        return false;
    }


    // fill the met
    TVector3 met3vec(met.Px(), met.Py(), met.Pz());
    group_inv->SetLabFrameThreeVector(met3vec);

    // fill the visible objects
    group_comb_vis->AddLabFrameFourVector(leptons.at(0));
    group_comb_vis->AddLabFrameFourVector(leptons.at(1));

    // run the Jigsaws now that we have the inputs loaded
    rf_lab->AnalyzeEvent();
    fill_variables();

    return true;
}


void CalculatorTTMET2LW::fill_variables()
{

    typedef TLorentzVector TLV;
    typedef TVector3 TV3;

    ///////////////////////////////////////
    // HT variables -- SS frame
    ///////////////////////////////////////
    TLV tlv_v1_ss = rf_v1->GetFourVector(*rf_ss);
    TLV tlv_v2_ss = rf_v2->GetFourVector(*rf_ss);
    TLV tlv_i1_ss = rf_i1->GetFourVector(*rf_ss);
    TLV tlv_i2_ss = rf_i2->GetFourVector(*rf_ss);

    TV3 p_v1_ss = tlv_v1_ss.Vect();
    TV3 p_v2_ss = tlv_v2_ss.Vect();
    TV3 p_i1_ss = tlv_i1_ss.Vect();
    TV3 p_i2_ss = tlv_i2_ss.Vect();

    TV3 p_v_ss = p_v1_ss + p_v2_ss;
    TV3 p_i_ss = p_i1_ss + p_i2_ss;

    float H_11_SS = p_v_ss.Mag() + p_i_ss.Mag();
    add_var("H_11_SS", H_11_SS);

    float H_21_SS = p_v1_ss.Mag() + p_v2_ss.Mag() + p_i_ss.Mag();
    add_var("H_21_SS", H_21_SS);

    float H_12_SS = p_v_ss.Mag() + p_i1_ss.Mag() + p_i2_ss.Mag();
    add_var("H_12_SS", H_12_SS);

    float H_22_SS = p_v1_ss.Mag() + p_v2_ss.Mag() + p_i1_ss.Mag() + p_i2_ss.Mag();
    add_var("H_22_SS", H_22_SS);

    ///////////////////////////////////////
    // HT variables -- S1 frame
    ///////////////////////////////////////
    TLV tlv_v1_s1 = rf_v1->GetFourVector(*rf_s1);
    TLV tlv_i1_s1 = rf_i1->GetFourVector(*rf_s1);

    TV3 p_v1_s1 = tlv_v1_s1.Vect();
    TV3 p_i1_s1 = tlv_i1_s1.Vect();

    float H_11_S1 = p_v1_s1.Mag() + p_i1_s1.Mag();
    add_var("H_11_S1", H_11_S1);

    ///////////////////////////////////////
    // Transverse Scale Variables
    ///////////////////////////////////////
    TV3 tp_v1_ss = tlv_v1_ss.Vect(); tp_v1_ss.SetZ(0.);
    TV3 tp_v2_ss = tlv_v2_ss.Vect(); tp_v2_ss.SetZ(0.);
    TV3 tp_i1_ss = tlv_i1_ss.Vect(); tp_i1_ss.SetZ(0.);
    TV3 tp_i2_ss = tlv_i2_ss.Vect(); tp_i2_ss.SetZ(0.);
    TV3 tp_v1_s1 = tlv_v1_s1.Vect(); tp_v1_s1.SetZ(0.);
    TV3 tp_i1_s1 = tlv_i1_s1.Vect(); tp_i1_s1.SetZ(0.);
    
    float H_11_SS_T = (tp_v1_ss + tp_v2_ss).Mag() + (tp_i1_ss + tp_i2_ss).Mag();
    add_var("H_11_SS_T", H_11_SS_T);

    float H_21_SS_T = tp_v1_ss.Mag() + tp_v2_ss.Mag() + (tp_i1_ss + tp_i2_ss).Mag();
    add_var("H_21_SS_T", H_21_SS_T);

    float H_22_SS_T = tp_v1_ss.Mag() + tp_v2_ss.Mag() + tp_i1_ss.Mag() + tp_i2_ss.Mag();
    add_var("H_22_SS_T", H_22_SS_T);

    float H_11_S1_T = tp_v1_s1.Mag() + tp_i1_s1.Mag();
    add_var("H_11_S1_T", H_11_S1_T);


    ///////////////////////////////////////
    // System Mass
    ///////////////////////////////////////
    float shat = rf_ss->GetMass();

    ///////////////////////////////////////
    // RATIOS
    ///////////////////////////////////////
    TV3 vPTT = rf_ss->GetFourVector(*rf_lab).Vect();
    float pTT_T = vPTT.Pt();
    add_var("pTT_T", pTT_T);

    float pTT_Z = vPTT.Pz();
    add_var("pTT_Z", pTT_Z);

    float RPT = vPTT.Pt() / (vPTT.Pt() + shat / 4.);
    add_var("RPT", RPT);

    float RPZ = std::abs( vPTT.Pz()) / (std::abs( vPTT.Pz()) + shat / 4.);
    add_var("RPZ", RPZ);

    float RPT_H_11_SS = vPTT.Pt() / (vPTT.Pt() + H_11_SS/4.);
    add_var("RPT_H_11_SS", RPT_H_11_SS);
    
    float RPT_H_21_SS = vPTT.Pt() / (vPTT.Pt() + H_21_SS/4.);
    add_var("RPT_H_21_SS", RPT_H_21_SS);

    float RPT_H_22_SS = vPTT.Pt() / (vPTT.Pt() + H_22_SS/4.);
    add_var("RPT_H_22_SS", RPT_H_22_SS);

    float RPZ_H_11_SS = fabs(vPTT.Pz()) / (fabs(vPTT.Pz()) + H_11_SS/4.);
    add_var("RPZ_H_11_SS", RPZ_H_11_SS);

    float RPZ_H_21_SS = fabs(vPTT.Pz()) / (fabs(vPTT.Pz()) + H_21_SS/4.);
    add_var("RPZ_H_21_SS", RPZ_H_21_SS);

    float RPZ_H_22_SS = fabs(vPTT.Pz()) / (fabs(vPTT.Pz()) + H_22_SS/4.);
    add_var("RPZ_H_22_SS", RPZ_H_22_SS);

    // trans
    float RPT_H_11_SS_T = vPTT.Pt() / (vPTT.Pt() + H_11_SS_T/4.);
    add_var("RPT_H_11_SS_T", RPT_H_11_SS_T);

    float RPT_H_21_SS_T = vPTT.Pt() / (vPTT.Pt() + H_21_SS_T/4.);
    add_var("RPT_H_21_SS_T", RPT_H_21_SS_T);

    float RPT_H_22_SS_T = vPTT.Pt() / (vPTT.Pt() + H_22_SS_T/4.);
    add_var("RPT_H_22_SS_T", RPT_H_22_SS_T);

    float RPZ_H_11_SS_T = fabs(vPTT.Pz()) / (fabs(vPTT.Pz()) + H_11_SS_T/4.);
    add_var("RPZ_H_11_SS_T", RPZ_H_11_SS_T);

    float RPZ_H_21_SS_T = fabs(vPTT.Pz()) / (fabs(vPTT.Pz()) + H_21_SS_T/4.);
    add_var("RPZ_H_21_SS_T", RPZ_H_21_SS_T);

    float RPZ_H_22_SS_T = fabs(vPTT.Pz()) / (fabs(vPTT.Pz()) + H_22_SS_T/4.); 
    add_var("RPZ_H_22_SS_T", RPZ_H_22_SS_T);

    ///////////////////////////////////////
    // SHAPES
    ///////////////////////////////////////
    float gamInvRp1 = rf_ss->GetVisibleShape();
    add_var("gamInvRp1", gamInvRp1);

    ///////////////////////////////////////
    // MDELTAR
    ///////////////////////////////////////
    float MDR = 2.0 * rf_v1->GetEnergy(*rf_s1);
    add_var("MDR", MDR);

    ///////////////////////////////////////
    // BOOST ANGLES
    ///////////////////////////////////////
    float dpb_vss = rf_ss->GetDeltaPhiBoostVisible();
    add_var("DPB_vSS", dpb_vss);

    ///////////////////////////////////////
    // ANGLES
    ///////////////////////////////////////
    float costheta_SS = rf_ss->GetCosDecayAngle();
    add_var("costheta_SS", costheta_SS);

    float dphi_v_SS = rf_ss->GetDeltaPhiVisible();
    add_var("dphi_v_SS", dphi_v_SS);

    // angle between invisible systems
    float dphi_v1_i1_ss = rf_v1->GetFourVector(*rf_ss).DeltaPhi(rf_i1->GetFourVector(*rf_ss));
    add_var("dphi_v1_i1_ss", dphi_v1_i1_ss);

    float dphi_s1_s2_ss = rf_s1->GetFourVector(*rf_ss).DeltaPhi(rf_s2->GetFourVector(*rf_ss));
    add_var("dphi_s1_s2_ss", dphi_s1_s2_ss);

    float dphi_S_I_ss = rf_s1->GetFourVector(*rf_ss).DeltaPhi(rf_i1->GetFourVector(*rf_ss));
    add_var("dphi_S_I_ss", dphi_S_I_ss);

    float dphi_S_I_s1 = rf_s1->GetFourVector(*rf_ss).DeltaPhi(rf_i1->GetFourVector(*rf_s1));
    add_var("dphi_S_I_s1", dphi_S_I_s1);

}

} // namespace jigsaw
